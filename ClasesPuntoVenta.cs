using System.ComponentModel.DataAnnotations;

public class Articulo
{
    [Key]
    public int sku { get; set; }
    public double Precio { get; set; }
    public int Puntos { get; set; }
    public string Nombre { get; set; }
    public string Cantidad { get; set; }

    /*Descuento*/
}
public class Caja
{
    [Key]
    public int IDCaja { get; set; }
    public double TipoCambio { get; set; }
    public double MontoInicial { get; set; }
    public double MontoActual { get; set; }
    
    public void Venta(double Monto, double Cambio) 
    {
        
    }
}

public class Cajero
{
    [Key]
    public int IDCajero { get; set; }
    public string Nombre { get; set; }
    public string Usuario { get; set; }
    public string Pass { get; set; }

    public void AsignarCaja()
    {

    }
    public void AbrirTurno()
    {

    }
    public void CerrarTurno()
    {

    }
}

public class Clientes
{
    [Key]
    public int IDCliente { get; set; }
    public string Nombre { get; set; }
    public string Telefono { get; set; }
    public string Direccion { get; set; }
    public int Puntos { get; set; }

    public void ConsultarPrecio (int sku)
    {
        
    }
}

public class Compra
{
    [Key]
    public int IDCompra { get; set; }
    public DateTime Fecha { get; set; }
    public int? IDCliente { get; set; } //
    public int IDCajero { get; set; }
    public List<Articulo> Carrito { get; set; }
    public double Total { get; set; }
    public bool Facturar { get; set; }

    public void AgregarArticulo(Articulo articulo)
    {

    }
    public void EliminarArticulo(Articulo articulo)
    {

    }

}

public class Inventario
{
    public List<Articulo> misArticulos{ get; set; }

    public void GuardarArticulo(Articulo articulo)
    {

    }
    public void EliminarArticulo(Articulo articulo)
    {

    }
    public void ModificarArticulo(Articulo articulo)
    {

    }
}
