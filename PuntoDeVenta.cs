public class Articulo
    {
        [Key]
        [Display(Name = "CodigoUnico")]
        public int sku { get; set; }

        [Required(ErrorMessage ="Ingrese el precio del articulo")]
        [DataType(DataType.Currency)]
        public double Precio { get; set; }

        [Required(ErrorMessage ="Ingrese los puntos disponibles del articulo")]
        public int Puntos { get; set; }

        [Required(ErrorMessage ="Ingrese el nombre del articulo")]
        [DataType(DataType.Text)]
        [StringLength(60, ErrorMessage = "Debe ser menor a 60 caracteres.")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Ingrese un numero valido")]
        public int Cantidad { get; set; }

        /*Descuento*/
    }

    public class Caja
    {
        [Key]
        public int IDCaja { get; set; }
        public double TipoCambio { get; set; }
        public double MontoInicial { get; set; }
        public double MontoActual { get; set; }
        
        public void Venta(double Monto, double Cambio) 
        {
            
        }
    }
    public class Cajero
    {
        [Key]
        public int IDCajero { get; set; }

        [DataType(DataType.Text)]
        public string Nombre { get; set; }
        [MinLength(6, ErrorMessage ="El usuario debe de contener mas de 6 caracteres.")]
        [StringLength(16,ErrorMessage ="El usuario debe de contener menos de 16 caracteres.")]
        public string Usuario { get; set; }

        [MinLength(4, ErrorMessage ="Ingrese una contraseña mayor a 50 caracteres.")]
        [StringLength(50,ErrorMessage ="Ingrese una contraseña menor a 50 caracteres.")]
        public string Pass { get; set; }

        public void AsignarCaja()
        {

        }
        public void AbrirTurno()
        {

        }
        public void CerrarTurno()
        {

        }
    }

    public class Clientes
    {
        [Key]
        public int IDCliente { get; set; }

        [StringLength(80, ErrorMessage = "El nombre debe de ser menor a 80 caracteres.")]
        public string Nombre { get; set; }
        [DataType(DataType.PhoneNumber, ErrorMessage ="Ingrese un numero correcto.")]
        public string Telefono { get; set; }

        public string Direccion { get; set; }

        public int Puntos { get; set; }

        public void ConsultarPrecio (int sku)
        {
            
        }
    }
    public class Compra
    {
        [Key]
        public int IDCompra { get; set; }

        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }

        public int? IDCliente { get; set; } //
        public int IDCajero { get; set; }
        public List<Articulo> Carrito { get; set; }

        [Required(ErrorMessage ="Ingrese el precio total de los articulos")]
        [DataType(DataType.Currency)]
        public double Total { get; set; }

        [Display(Name ="Facturar compra")]
        [Range(typeof(bool), "Si", "No", ErrorMessage ="Se debe de especificar esta seccion.")]
        public bool Facturar { get; set; }

        public void AgregarArticulo(Articulo articulo)
        {

        }
        public void EliminarArticulo(Articulo articulo)
        {

        }

    }